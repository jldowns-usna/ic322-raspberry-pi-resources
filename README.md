# Raspberry Pi Resources for IC322

## Network Settings

By default, the pis are configured with static ip addresses:

```
IPv4:    192.168.1.11
Mask:    255.255.255.0
Gateway: 192.168.1.1
```

This way you can connect directly to them using an ethernet cable and set your wired interface to match the network subnet. I'll often set my computer to:

```
IPv4:    192.168.1.20
Mask:    255.255.255.0
Gateway: <blank>
```

Setting the gateway blank means that traffic destined outside the 192.168.1.0/24 subnet will not be sent to this interface.

