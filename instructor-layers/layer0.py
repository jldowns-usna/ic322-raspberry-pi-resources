#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import getopt,sys

# initialized board pins
GPIO.setmode(GPIO.BOARD)

class Interface:
    """Maintains the state of the interface."""

    def __init__(self, input_pin, output_pin, clock_interval):
        # set pin modes
        self.input_pin = input_pin
        self.output_pin = output_pin
        print(f"setting {self.input_pin} as input.")
        print(f"setting {self.output_pin} as output.")
        GPIO.setup(self.input_pin, GPIO.IN)
        GPIO.setup(self.output_pin, GPIO.OUT)

        # Keep track of the last time we saw the edge of a signal.
        # initialize to current time.
        self.time_last_edge_received = time.time_ns()
        self.clock_interval = clock_interval # in ns

        # keep track of output state.
        # 0 = False, 1 = True
        self.output_value = False

        return

    def receive_edge(self, channel):
        """Called on the rising edge of a signal."""
        # Measure the length of the last signal. If it was 1 clock_interval,
        # it's a 1. If it was 2 clock_intervals, it's a 0.
        signal_length = time.time_ns() - self.time_last_edge_received
        intervals = round(abs(signal_length/self.clock_interval))
        self.time_last_edge_received = time.time_ns()
        if intervals == 1:
            print("1")
        elif intervals == 2:
            print("0")
        else:
            print(f"Received edge after {signal_length}. Clock interval set to {self.clock_interval}. That's {intervals} intervals.")
        # we do nothing if the signal was much less than a single clock
        # interval (noise) or much greater than 2 intervals (restart from idle).


    def send_and_block(self, data):
        """Sends data over the interface, but blocks while it does so.
        `data` should be a List of 1,0 values."""
        GPIO.output(self.output_pin, self.output_value)
        for d in data:
            self.output_value = not self.output_value
            print(f"setting {self.output_pin} to {self.output_value} ({d})")
            GPIO.output(self.output_pin, self.output_value)
            if d == 1:
                time.sleep(self.clock_interval/1000000000)
            if d == 0:
                time.sleep(self.clock_interval/1000000000 * 2)

        self.output_value = not self.output_value
        GPIO.output(self.output_pin, self.output_value)

