from layer0 import Interface
import RPi.GPIO as GPIO

print ("Starting receiver...")

interface = Interface(11, 13, 10**7)
GPIO.add_event_detect(11, GPIO.BOTH, callback=interface.receive_edge)

while True:
    pass
